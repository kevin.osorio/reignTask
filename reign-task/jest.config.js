module.exports = {
  projects: [
    '<rootDir>/apps/reign-front',
    '<rootDir>/apps/api',
    '<rootDir>/libs/data',
  ],
  testEnvironment: 'node'
};
