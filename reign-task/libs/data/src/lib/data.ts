export function data(): string {
  return 'data';
}
export interface News  {
  _id?: number;
   title: string;  
   author: string;  
   created_at: Date;
   isAvailable: boolean;
   url: string;
   story_id: number;
}


