import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { News } from "@reign-task/data";
import axios from 'axios'
import classes from './newsList.module.css'
import { NewsItem } from './news/news';

export const App = () => {
  const [news, setNews] = useState<News[]>([]);

  useEffect(() => {
    axios.get('http://localhost:3000/news').then((response) => {
      const orderedNews = response.data.sort(function (a, b) {

        return Date.parse(b.created_at) - Date.parse(a.created_at);
      })
      console.log(response)
      setNews(orderedNews)
    })
  }, []);

  const handleDeleteNews = useCallback((newNews: News) => {
    try {
      const disableNews = { ...newNews, isAvailable: false };
      axios.put(`http://localhost:3000/news/${newNews._id}`, disableNews).then(response => {
        console.log(response)
        const newNews = news.map((oldNews) => {
          if (oldNews._id === response.data._id) {
            oldNews = { ...response.data };
          }
          return oldNews;
        })
        setNews(newNews)
      });
    } catch (err) { console.error(err); }
  }, [news])

  const redirect = useCallback((url: string) => {
    if (url !== null) {
      window.open(url, '_blank')
    }
  }, [])

  const table = useMemo(() => (news.map((news, index) => (
    news.title !== null && news.isAvailable ? (
      <NewsItem redirect={redirect} key={index} news={news} handleDelete={handleDeleteNews} index={index} />
    ) : null
  ))), [handleDeleteNews, news, redirect])

  return (
    console.log(news),
    <>
      <header className={classes.header}>
        <h2>
          HN Feed
        </h2>
        <h4>
          {"We <3 hacker news!"}
        </h4>
      </header>
      <div></div>
      <body>
        <table className={classes.table}>
          <tbody className={classes.body}>
            {news.map((news, index) => (
              news.title !== null && news.isAvailable ? (
                <NewsItem redirect={redirect} key={index} news={news} handleDelete={handleDeleteNews} index={index} />
              ) : null))}
          </tbody>
        </table>
      </body>
    </>
  );
};

export default App;