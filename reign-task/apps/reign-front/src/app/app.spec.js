
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var react_2 = require("@testing-library/react");
var app_1 = __importDefault(require("./app"));
describe('App', function () {
    it('should render successfully', function () {
        var baseElement = react_2.render(<app_1.default />).baseElement;
        expect(baseElement).toBeTruthy();
    });
    it('should have a greeting as the title', function () {
        var getByText = react_2.render(<app_1.default />).getByText;
        expect(getByText('Welcome to reign-front!')).toBeTruthy();
    });
});
