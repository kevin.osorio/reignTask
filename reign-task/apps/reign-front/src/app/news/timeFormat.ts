export function timeFormat(time: Date): string {
  const today = new Date()
  const day = new Date(time);

  return dayStr(day,today);
}

function month(month: number): string {
  switch (month) {
    case 0: return "Jan"
    case 1: return "Feb"
    case 2: return "Mar"
    case 3: return "Apr"
    case 4: return "May"
    case 5: return "Jun"
    case 6: return "Jul"
    case 7: return "Aug"
    case 8: return "Sep"
    case 9: return "Oct"
    case 10: return "Nov"
    case 11: return "Dec"
  }
}
function dayStr(day: Date, today: Date): string {
  if (today.getFullYear === day.getFullYear) {
    if (today.getMonth() === day.getMonth()) {
      if (today.getDate() === day.getDate()) {
        return `${today.getHours()} : ${today.getMinutes()}`;
      } else if (today.getDate() - day.getDate() === 1) {
        return "Yesterday";
      }
    }
  }
  return `${month(day.getMonth())} ${day.getDate()}`;

}