import React, { useState } from 'react';
import classes from '../newsList.module.css'
import './news.module.css';
import { News } from "@reign-task/data";
import { timeFormat } from './timeFormat'
/* eslint-disable-next-line */


export function NewsItem(props) {
  const algo = 5;
  const [trashEnabled, setTrashEnabled] = useState<boolean>(false)
  const [mouseOnTrash,setMouseOnTrash] = useState<boolean>(false)

  function handleOnMouseEnter() {
    setTrashEnabled(true)
  }
  function handleOnMouseLeave() {
    setTrashEnabled(false)
  }
  function handleOnMouseEnterTrash() {
    setMouseOnTrash(true)
  }
  function handleOnMouseLeaveTrash() {
    setTrashEnabled(false)
  }

  function deleteNews() {
      handleOnMouseLeave()
      props.handleDelete(props.news)
  }

  return (
    <tr className={classes.tr}  onClick={() => mouseOnTrash? null:props.redirect(props.news.url)} onMouseEnter={handleOnMouseEnter} onMouseLeave={handleOnMouseLeave}>
      <td className={classes.td1} >
        {props.news.title}
      </td>
      <td className={classes.author} >
        - {props.news.author}
      </td>
      <td className={classes.time}>
        - {timeFormat(props.news.created_at)}
      </td>
      <td onMouseEnter={handleOnMouseEnterTrash} onMouseLeave={handleOnMouseLeaveTrash} style={{ width: 32 }}>
        {trashEnabled ? (<button onClick={deleteNews} style={{ zIndex: 100000 }}><span role="img" aria-label="trash" style={{ zIndex: 100 }}> 🗑️</span> </button>) : null}
      </td>
    </tr>

  )
}

export default News;
