import {  HttpModule, Module } from '@nestjs/common';
import { NewsController } from './news.controller';
import { newsProviders } from '../app/providers/news,providers';
import {NewsSchema } from './entities/news.schema'
import { MongooseModule } from '@nestjs/mongoose';
import { NewsService } from './news.service';
import { DatabaseModule } from '../app/database.module';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'News', schema: NewsSchema }]),HttpModule,DatabaseModule],
  controllers: [NewsController],
  providers: [...newsProviders,NewsService]
})
export class NewsModule {}
