import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete, Put } from '@nestjs/common';
import { ApiTags, ApiResponse } from '@nestjs/swagger';
import { News } from '@reign-task/data';
import { CreateNewsDto } from './dto/create-news.dto';
import { NewsService } from './news.service';

@ApiTags('news')
@Controller('news')
export class NewsController {
  constructor(private readonly newsService: NewsService) { }
  @Get()
  public async getNews(@Response() res,): Promise<News[]>{

    const news = await this.newsService.findAll();
    const savedNews = news.length === 0 ? await this.newsService.findAll() : news;

    return res.status(HttpStatus.OK).json(savedNews);
  }

  @Get('find')
  public async findNews(@Response() res, @Body() body) {
    const queryCondition = body;
    const news = await this.newsService.findOne(queryCondition);
    return res.status(HttpStatus.OK).json(news);
  }

  @Get('/:id')
  public async getNew(@Response() res, @Param() param) {
    const news = await this.newsService.findById(param.id);
    return res.status(HttpStatus.OK).json(news);
  }

  @Post()
  @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  public async createNews(@Response() res, @Body() createNewsDTO: CreateNewsDto) {

    const news = await this.newsService.create(createNewsDTO);
    return res.status(HttpStatus.OK).json(news);
  }

  @Put('/:id')
  public async updateNews(@Param() param, @Response() res, @Body() body) {

    const news = await this.newsService.update(param.id, body);
    return res.status(HttpStatus.OK).json(news);
  }

  @Delete('/:id')
  public async deleteNews(@Param() param, @Response() res) {

    const news = await this.newsService.delete(param.id);
    return res.status(HttpStatus.OK).json(news);
  }
}