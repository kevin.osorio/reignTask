import * as mongoose from 'mongoose';

export const NewsSchema = new mongoose.Schema({
  title: String,
  author: String,
  created_at: Date,
  isAvailable: Boolean,
  story_id: Number,
  url: String
})


