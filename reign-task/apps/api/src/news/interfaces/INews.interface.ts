import { Document } from 'mongoose';
export interface INews extends Document {
  readonly title: string;  
  readonly author: string;  
  readonly created_at: Date;
  readonly isAvailable: boolean;
  readonly story_id: number;
  readonly url: string;

}