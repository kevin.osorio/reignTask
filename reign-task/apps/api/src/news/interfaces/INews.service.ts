// import { HttpService, Injectable } from '@nestjs/common';
// import { InjectModel } from '@nestjs/mongoose';
// import { Interval } from '@nestjs/schedule';
// import { Model } from 'mongoose';
// import { UpdateNewsDto } from './dto/update-news.dto';
// import {News} from "./entities/news.interface"

import { News } from "@reign-task/data";
import { AxiosResponse } from "axios";
import { CreateNewsDto } from "../dto/create-news.dto";
import { INews } from "./INews.interface";


// @Injectable()
// export class NewsService {
//   constructor(@InjectModel(News.name)
//   private newsModel: Model<News>,
//     private httpService: HttpService) { }

//   parserNews(hit): News {
//     const news = {
//       title: hit.story_title !== null ?
//         hit.story_title :
//         hit.title !== null ?
//           hit.title :
//           null,
//       author: hit.author,
//       created_at: hit.created_at,
//       story_id: hit.story_id,
//       isAvailable: false
//     };
//     news.isAvailable = news.title !== null ? true : false
//     return news
//   }

//   @Interval(3600000)
//   async fillDataBase(){
//     const data = await this.httpService.get("https://hn.algolia.com/api/v1/search_by_date?query=nodejs").toPromise();
//     data.data.hits.map((hit) => {
//       const savedNews = this.newsModel.findOne({ "story_id": hit.story_id })
//       if (!savedNews) {
//         const newNews = this.parserNews(hit)
//         return this.create(newNews)
//       }
//     })
//   }

//   create(createNewsDto: News): Promise<News> {
//     const createdNews = new this.newsModel(createNewsDto);
//     return createdNews.save();
//   }

//   async findAll(): Promise<News[]> {
//     const storedNews = await this.newsModel.find();
//     return storedNews
//   }

//   async update(id: number, updateNewsDto: UpdateNewsDto) {
//     const updatedNews = await this.newsModel.findById(id)
//     return updatedNews.update(updateNewsDto)
//   }
// }



export interface INewsService {
    findAll(): Promise<News[]>;
    findById(ID: number): Promise<News | null>;
    // eslint-disable-next-line @typescript-eslint/ban-types
    findOne(options: object): Promise<News | null>;
    create(createNewsDto: CreateNewsDto): Promise<News>;
    update(ID: number, newValue: News): Promise<News | null>;
    delete(ID: number): Promise<string>;
}