import { ApiProperty } from '@nestjs/swagger';
export class CreateNewsDto {
  @ApiProperty()
  readonly title: string;
  @ApiProperty()
  readonly author: string;
  @ApiProperty()
  readonly created_at: Date;
  @ApiProperty()
  readonly isAvailable: boolean;
  @ApiProperty()
  readonly story_id: number;
  @ApiProperty()
  readonly url: string;
}