import { Model } from 'mongoose';
import { HttpService, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { INewsService, INews } from './interfaces/index';
import { debug } from 'console';
import { CreateNewsDto } from './dto/create-news.dto';
import { Interval } from '@nestjs/schedule';
import * as operators from "rxjs/operators";
import { News } from "@reign-task/data"

@Injectable()
export class NewsService implements INewsService {
  constructor(@InjectModel('News') private readonly newsModel: Model<INews>, private readonly httpService: HttpService) { }
  private readonly logger = new Logger(NewsService.name)


  async findAll(): Promise<News[]> {
    let savedNews = await this.newsModel.find();
    if (savedNews.length === 0) {
      await this.fillDataBase()
      savedNews = await this.newsModel.find().exec();
    }

    return savedNews;
  }

   parserNews(hit): News {
    const title =  (hit.story_title !== null ?
      hit.story_title :
      hit.title !== null ?
        hit.title :
        null)
    const isAvailable =  (title !== null ? true : false)
    const url =  (hit.story_url !== null ? hit.story_url : hit.url !== null ? hit.url : null)

    const news: News = {
      title: title,
      author: hit.author,
      url: url,
      isAvailable: isAvailable,
      created_at: hit.created_at,
      story_id: hit.story_id,
    };
    this.logger.log(news.url)
    return news
  }

  async apiCall(page: number) {
    return await this.httpService.get(`https://hn.algolia.com/api/v1/search_by_date?query=nodejs&page=${page}`, {
      headers: {
        'Accept': 'application/json'
      }
    }).pipe(
      operators.map(response => response.data),
    ).toPromise();
  }

  @Interval(3600000)
  async fillDataBase() {
    const data = await this.apiCall(0);
    let hits = data.hits;
    for (let i = 0; i < data.nbPages; i++) {
      const data = await this.apiCall((i + 1));
      this.logger.log(i);
      hits = await hits.concat(data.hits)
    }
    hits.map(async (hit) => {
      const savedNews = await this.findOne({ story_id: Number(hit.story_id) })
      if (!savedNews) {
        const newNews: News = await this.parserNews(hit)
        this.logger.log(newNews.url)
        await this.create(await {...newNews})
        this.logger.log(newNews)
      }
    })
  }


  // eslint-disable-next-line @typescript-eslint/ban-types
  async findOne(options: object): Promise<News> {
    return await this.newsModel.findOne(options).exec();
  }

  async findById(ID: number): Promise<News> {
    return await this.newsModel.findById(ID).exec();
  }
  async create(createNewsDto: CreateNewsDto): Promise<News> {
    const createdNews = new this.newsModel(createNewsDto);
    return await createdNews.save();
  }

  async update(ID: number, newValue: News): Promise<News> {
    const news = await this.newsModel.findById(ID).exec();

    if (!news._id) {
      debug('news not found');
    }

    await this.newsModel.findByIdAndUpdate(ID, newValue);
    return await this.newsModel.findById(ID).exec();
  }
  async delete(ID: number): Promise<string> {
    try {
      await this.newsModel.findByIdAndRemove(ID).exec();
      return 'The news has been deleted';
    }
    catch (err) {
      debug(err);
      return 'The news could not be deleted';
    }
  }
}