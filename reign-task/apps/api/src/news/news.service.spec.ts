import { Test, TestingModule } from '@nestjs/testing';
import { MongooseModule } from '@nestjs/mongoose';
import { closeInMongodConnection, rootMongooseTestModule } from '../app/rootMongooseTestModule';
import { NewsSchema } from './entities/news.schema';
import { NewsService } from './news.service';
import { HttpService } from '@nestjs/common';
import {NewsModule} from "./news.module"

describe('NewsService', () => {
  let service: NewsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        rootMongooseTestModule(),NewsModule,
        MongooseModule.forFeature([{ name: 'News', schema: NewsSchema }])
      ],
      providers: [NewsService],
    }).compile();

    service = module.get<NewsService>(NewsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  /**
    Write meaningful test
  **/

  afterAll(async () => {
    await closeInMongodConnection();
  });
});